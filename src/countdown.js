(function ($) {
   'use strict';
    var pluginName = 'countDown';
    $[pluginName] = (function () {
        /**
         * Plugin Constructor. This function build the basic object for the plugin
         * @param (object) element - The jQuery or Zepto DOM element
         * @param (object) options - A list of options for the plugin
         */
        $[pluginName] = function (element, options) {

            var defaults = {
                $offerEndDate: new Date()
            };
            this.options        = $.extend({}, defaults, options);
            this._defaults      = defaults;
            this.$element       = $(element);
            this.$fTime         = this.options.time
            this.$flashmessage  = this.options.offlineSale;
            // init plugin
            return this._Counter();
        };
        $[pluginName].prototype = {

            _Counter: function () {
                var _this = this;
                if (_this.$fTime) {
                    var timer = function(){
                    var time2Offer            = _this.$fTime--; // Time in seconds that I get in my data-counter param.
                    var days                  = Math.floor(time2Offer / 86400);
                    var hours                 = Math.floor(((time2Offer % 31536000) % 86400) / 3600);
                    var dividetime2Offer      = time2Offer % (60 * 60);
                    var minutes               = Math.floor(dividetime2Offer / 60);
                    var divisor_for_seconds   = dividetime2Offer % 60;
                    var seconds               = Math.ceil(divisor_for_seconds);

                    var time = {
                        "d": days,
                        "h": hours,
                        "m": minutes,
                        "s": seconds
                    };
                    time.d = (time.d < 1) ? time.d = '' : time.d + 'j ';
                    time.h = (time.h < 1) ? time.h = '' : time.h +'h ';
                    time.m = (time.m < 1) ? time.m = '' : time.m + 'm ';
                    // Write counter
                     _this.$element.html(time.d + time.h + time.m + time.s + 's ');

                      // Show message when promotion is ended
                      if(time.d == 0 && time.h == 0 && time.m == 0 && time.s == 0){
                          clearInterval(counter);
                              _this.$element.html(_this.$flashmessage);
                      }
                  }
                  var counter = setInterval(function () {
                      timer();
                  }, 1000);
                }
            }
          };
        // Building the plugin
        /**
         * The plugin component
         * @param  {object} options - list of all parameters for the jQuery/Zepto module
         * @return {object} - The jQuery/Zepto DOM element
         */
        return $[pluginName];
    }(window));

    $.fn[pluginName] = function (options) {
        return this.each(function () {
            if (!$(this).data(pluginName)) {
                if (options === 'destroy') {
                    return;
                }
                $(this).data(pluginName, new $[pluginName](this, options));
            } else {
                var $plugin = $(this).data(pluginName);
            }
        });
    };

})(window.Zepto || window.jQuery);
