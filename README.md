CountDown
================================

Zepto and jQuery Plugin to count down to any date

Options :
---------

	time 								 Default : true 				Type : Date (seconds)
    offlineSale                          Default : 'Your message'       Type : String
